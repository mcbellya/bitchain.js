(function (global) {
  'use strict';
  
  var BITS_PER_INT = 32,
      LEADING_ZEROS = (new Array(BITS_PER_INT + 1)).join('0');
  
  /**
   * Identity function
   * @private
   * @param   {Any}      value Any value to be returned
   * @returns {Function} Function returning the value
   */
  function identity(value) {
    return function () {
      return value;
    };
  }
  
  /**
   * BitChain object. Provides basic functionality for easily
   * operating the bit chains of the specified length.
   * @constructor
   * @name BitChain
   * @param   {Number}  length Amount of bits in a chain
   * @param   {Boolean} ones   Specifies whether the chain should be
   *                         filled with "1"
   */
  function BitChain(length, ones) {
    var initialValue = ones ? 0xFFFFFFFF : 0,
        numbersAmount = Math.floor(length / BITS_PER_INT) + ((length % BITS_PER_INT) ? 1 : 0);
    
    if (global.Uint32Array) {
      this.__data = (function (data) {
        for (var i = 0; i < numbersAmount; i++) {
          data[i] = initialValue;
        }
        return data;
      }(new global.Uint32Array(numbersAmount)));
    } else {
      this.__data  = Array.apply(null, new Array(numbersAmount)).map(identity(initialValue));
    }
    
    /**
     * Getter for bit chain length
     * @returns {Number} Length of the bit chain
     */
    this.length = function () {
      return length;
    };
    
    /**
     * Getter for bit chain numbers amount
     * @returns {Number} Amount of integer numbers used in bit chain
     */
    this.numbersAmount = function () {
      return numbersAmount;
    };
    
  }
  
  BitChain.prototype.constructor = BitChain;
  
  /**
   * Sets the specified bit to 1
   * @memberof BitChain
   * @param   {Number} bitNumber Bit number to be set
   * @returns {Object} Reference to the current bit chain
   */
  BitChain.prototype.setBit = function (bitNumber) {
    return this._bitOperation(bitNumber, function (numberIndex, bitNumber) {
      return (this.__data[numberIndex] |= 1 << bitNumber), this;
    });
  };

  /**
   * Drops the specified bit to 0
   * @memberof BitChain
   * @param   {Number} bitNumber Bit number to be dropped
   * @returns {Object} Reference to the current bit chain
   */
  BitChain.prototype.dropBit = function (bitNumber) {
    return this._bitOperation(bitNumber, function (numberIndex, bitNumber) {
      return (this.__data[numberIndex] &= ~0 ^ 1 << bitNumber), this;
    });
  };

  /**
   * Gets the value of a bit
   * @memberof BitChain
   * @param   {Number} bitNumber Number of a bit to get value from
   * @returns {Number} Value of the bit
   */
  BitChain.prototype.getBit = function (bitNumber) {
    return this._bitOperation(bitNumber, function (numberIndex, bitNumber) {
      return (this.__data[numberIndex] & (1 << bitNumber)) >>> bitNumber;
    });
  };

  /**
   * Helper to perform a operations with single bit
   * @memberof BitChain
   * @private
   * @param   {Number}        bitNumber Number of a bit to operate with
   * @param   {Function}      operation Operation to be performed
   * @returns {Object|Number} Result of the operation
   */
  BitChain.prototype._bitOperation = function (bitNumber, operation) {
    var index = Math.floor(bitNumber / BITS_PER_INT),
        bit = bitNumber % BITS_PER_INT;

    if (bitNumber > (this.length() - 1)) {
      throw new RangeError('bit number is bigger then bit chain length');
    }

    return operation.call(this, index, bit);
  };

  /**
   * Performs bitwise "AND" operation betwee the current bit chain
   * and passed value
   * @memberof BitChain
   * @param   {Object|Number} value Either a bit chain or integer number 
   *                                to operate with
   * @returns {Object}        New instance of the BitChain object with
   *                          resulting value
   */
  BitChain.prototype.and = function (value) {
    return this._binaryOperation(value, function (value1, value2) {
      return value1 & value2;
    });
  };

  /**
   * Performs bitwise "OR" operation betwee the current bit chain
   * and passed value
   * @memberof BitChain
   * @param   {Object|Number} value Either a bit chain or integer number
   *                                to operate with
   * @returns {Object}        New instance of the BitChain object with
   *                          resulting value
   */
  BitChain.prototype.or = function (value) {
    return this._binaryOperation(value, function (value1, value2) {
      return value1 | value2;
    });
  };

  /**
   * Performs bitwise "XOR" operation betwee the current bit chain
   * and passed value
   * @memberof BitChain
   * @param   {Object|Number} value Either a bit chain or integer
   *                                number to operate with
   * @returns {Object}        New instance of the BitChain object
   *                          with resulting value
   */
  BitChain.prototype.xor = function (value) {
    return this._binaryOperation(value, function (value1, value2) {
      return value1 ^ value2;
    });
  };

  /**
   * Helper function to perform a binary operation
   * @memberof BitChain
   * @private
   * @param   {Object|Number} value     Either a bit chain or integer
   *                                    number to operate with
   * @param   {Function}      operation Operation to be performed
   * @returns {Object}        Result of the operation
   */
  BitChain.prototype._binaryOperation = function (value, operation) {
    var result = new BitChain(this.length()),
        valueLength = (value instanceof BitChain) ? value.length() : BITS_PER_INT,
        valueDataLength = (value instanceof BitChain) ? value.__data.length : 1,
        i;

    if (this.length() < valueLength) {
      throw new RangeError('value length should be not bigger then bit chain length');
    }

    for (i = 0; i < this.numbersAmount(); i++) {
      result.__data[i] = this.__data[i];
    }

    if (value instanceof BitChain) {
      for (i = 0; i < valueDataLength; i++) {
        result.__data[i] = operation(this.__data[i], value.__data[i]);
      }
    } else if (typeof value === 'number') {
      result.__data[0] = operation(this.__data[0], value);
    } else {
      throw new TypeError('value should be either instance of BitChain or number');
    }

    for (i = valueDataLength; i < this.numbersAmount(); i++) {
      result.__data[i] = operation(this.__data[i], 0);
    }

    return result;
  };

  /**
   * Performs bitwise "NOT" operation
   * @memberof BitChain
   * @returns {Object} New instance of the BitChain object with resulting value
   */
  BitChain.prototype.not = function () {
    var result = new BitChain(this.length());
    for (var i = 0; i < this.numbersAmount(); i++) {
       result.__data[i] = ~(this.__data[i] >>> 0);
    }
    return result;
  };

  /**
   * Performs left shifting
   * @memberof BitChain
   * @param {Number}   bits Number of bits to shift
   * @returns {Object} New instance of the BitChain object with resulting value
   */
  BitChain.prototype.shl = function (bits) {
    var result = new BitChain(this.length()), i;

    for (i = 0; i < this.numbersAmount(); i++) {
      result.__data[i] = this.__data[i] << bits;
    }

    for (i = 0; i < this.numbersAmount() - 1; i++) {
      result.__data[i + 1] |= this.__data[i] >>> (BITS_PER_INT - bits);
    }

    return result;
  };

  /**
   * Performs right shifting
   * @memberof BitChain
   * @param {Number}   bits Number of bits to shift
   * @returns {Object} New instance of the BitChain object with resulting value
   */
  BitChain.prototype.shr = function (bits) {
    var result = new BitChain(this.length()), i;

    for (i = 0; i < this.numbersAmount(); i++) {
      result.__data[i] = this.__data[i] >>> bits;
    }

    for (i = 0; i < this.numbersAmount() - 1; i++) {
      result.__data[i] |= this.__data[i + 1] << (BITS_PER_INT - bits);
    }

    return result;
  };

  /**
   * Checks whether all bits in current bit chain are dropped
   * @memberof BitChain
   * @returns {Boolean} *'true'* if all the bits are equal to '0'
   */
  BitChain.prototype.allDropped = function () {
    for (var i = 0; i < this.numbersAmount(); i++) {
      if (this.__data[i] !== 0x0) {
        return false;
      }
    }
    return true;
  };

  /**
   * Checks whether all bits in current bit chain are set
   * @memberof BitChain
   * @returns {Boolean} *'true'* if all the bits are equal to '1'
   */
  BitChain.prototype.allSet = function () {
    for (var i = 0; i < this.numbersAmount(); i++) {
      if (this.__data[i] !== 0xFFFFFFFF) {
        return false;
      }
    }
    return true;
  };

  /**
   * Compares two bit chains for equality.
   * 
   * **Two bit chains** are equal when:
   * 1. They have equal length;
   * 2. Correspondent bits have equal values.
   * 
   * **Bit chain and number** are equal when:
   * 1. Length of the current bit chain not bigger then 32 bits;
   * 2. Correspondent bits have equal values.
   * 
   * @memberof BitChain
   * @param   {Object|Number} value Either an instance of BitChain object
   *                              or integer number
   * @returns {Boolean}       *'true'* if bit chains are equal
   */
  BitChain.prototype.equals = function (value) {
    if (value instanceof BitChain) {
      if (value.length() != this.length()) {
        return false;
      }

      return this.xor(value).allDropped();
    } else if (typeof value === 'number') {
      if (this.length() > BITS_PER_INT) {
        return false;
      }

      return this.__data[0] === value;
    }

    return false;
  };

  /**
   * Converts a bit chain to string
   * @memberof BitChain
   * @param   {Boolean} beautify Whether the resulting string
   *                             should be beautified (all bytes
   *                             are separated with spaces)
   * @returns {String}  String representation of the bit chain
   */
  BitChain.prototype.toString = function (beautify) {
    var value = '', str;
    for (var i = 0; i < this.numbersAmount(); i++) {
      str = (this.__data[i] >>> 0).toString(2);
      value = LEADING_ZEROS.substr(0, BITS_PER_INT - str.length) + str + value;
    }

    value = value.substr(-this.length());

    if (beautify) {
      value = value.split('').reverse().join('')  // reverse value
                   .match(/.{1,8}/g).join(' ')    // split bytes with spaces
                   .split('').reverse().join(''); // reverse back
    }

    return value;
  };
  
  global.BitChain = BitChain;
  
}(this));