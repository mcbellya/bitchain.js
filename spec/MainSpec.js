/* global BitChain, describe, it, expect, beforeEach */
describe ('BitChain', function () {
  var BIT_CHAIN_LENGTH = 40;
  
  it ('should be filled with zeros', function () {
    var bitChain = new BitChain(BIT_CHAIN_LENGTH);
    expect(bitChain.allDropped()).to.be.true;
  });
  
  it ('should be filled with ones', function () {
    var bitChain = new BitChain(BIT_CHAIN_LENGTH, true);
    expect(bitChain.allSet()).to.be.true;
  });
  
  it ('should have proper length', function () {
    var bitChain = new BitChain(BIT_CHAIN_LENGTH);
    expect(bitChain.length()).to.equal(BIT_CHAIN_LENGTH);
  });
  
  it ('should have proper numbers amount', function () {
    var NUMBERS_AMOUNT = 2,
        bitChain = new BitChain(BIT_CHAIN_LENGTH);
    expect(bitChain.numbersAmount()).to.equal(NUMBERS_AMOUNT);
  });
  
  describe ('bit operations', function () {
    
    beforeEach(function () {
      this.bitChain = new BitChain(BIT_CHAIN_LENGTH);
      this.bitChain = this.bitChain.or(9);
    });
    
    it ('getBit', function () {
      expect(this.bitChain.getBit(2)).to.equal(0);
      expect(this.bitChain.getBit(3)).to.equal(1);
    });
    
    it ('setBit', function () {
      expect(this.bitChain.getBit(2)).to.equal(0);
      expect(this.bitChain.setBit(2).getBit(2)).to.equal(1);
    });
    
    it ('dropBit', function () {
      expect(this.bitChain.getBit(3)).to.equal(1);
      expect(this.bitChain.dropBit(3).getBit(3)).to.equal(0);
    });
    
  });
  
  describe ('unary bitwise operations', function () {
    
    beforeEach(function () {
      this.bitChain = new BitChain(BIT_CHAIN_LENGTH);
    });
    
    it ('not', function () {
      expect(this.bitChain.allDropped()).to.be.true;
      expect(this.bitChain.not().allSet()).to.be.true;
    });
    
    describe ('shift operations', function () {
      
      beforeEach(function () {
        this.bits = 1;
        this.bitChain.setBit(32).setBit(31);
      });
      
      it ('shl', function () {
        var result = this.bitChain.shl(this.bits);
        expect(result.getBit(31)).to.equal(0);
        expect(result.getBit(32)).to.equal(1);
        expect(result.getBit(33)).to.equal(1);
      });
      
      it ('shr', function () {
        var result = this.bitChain.shr(this.bits);
        expect(result.getBit(30)).to.equal(1);
        expect(result.getBit(31)).to.equal(1);
        expect(result.getBit(32)).to.equal(0);
      });
      
    });
    
  });
  
  describe ('binary bitwise operations', function () {
    
    beforeEach(function () {
      this.bitChain1 = new BitChain(BIT_CHAIN_LENGTH).setBit(33).setBit(3);
      this.bitChain2 = new BitChain(BIT_CHAIN_LENGTH).setBit(25).setBit(5).setBit(3);
    });
    
    describe ('and', function () {
      
      it ('with bit chain', function () {
        var result = this.bitChain1.and(this.bitChain2);
        expect(result.getBit(3)).to.equal(1);
        expect(result.getBit(5)).to.equal(0);
        expect(result.getBit(25)).to.equal(0);
        expect(result.getBit(33)).to.equal(0);
      });
      
      it ('with number', function () {
        var result = this.bitChain1.and(10);
        expect(result.getBit(2)).to.equal(0);
        expect(result.getBit(3)).to.equal(1);
        expect(result.getBit(33)).to.equal(0);
      });
      
    });
    
    describe ('or', function () {
      
      it ('with bit chain', function () {
        var result = this.bitChain1.or(this.bitChain2);
        expect(result.getBit(3)).to.equal(1);
        expect(result.getBit(5)).to.equal(1);
        expect(result.getBit(25)).to.equal(1);
        expect(result.getBit(33)).to.equal(1);
      });
      
      it ('with number', function () {
        var result = this.bitChain1.or(10);
        expect(result.getBit(1)).to.equal(1);
        expect(result.getBit(3)).to.equal(1);
        expect(result.getBit(33)).to.equal(1);
      });
      
    });
    
    describe ('xor', function () {
      
      it ('with bit chain', function () {
        var result = this.bitChain1.xor(this.bitChain2);
        expect(result.getBit(3)).to.equal(0);
        expect(result.getBit(5)).to.equal(1);
        expect(result.getBit(25)).to.equal(1);
        expect(result.getBit(33)).to.equal(1);
      });
      
      it ('with number', function () {
        var result = this.bitChain1.xor(10);
        expect(result.getBit(1)).to.equal(1);
        expect(result.getBit(3)).to.equal(0);
        expect(result.getBit(33)).to.equal(1);
      });
      
    });
    
  });
  
});